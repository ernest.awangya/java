FROM openjdk:8-jre-alpine
RUN mkdir /usr/app
COPY . /usr/app
WORKDIR /usr/app
EXPOSE 8080
CMD java -jar target/maven-web-app.war
